# Docker Grav

This project builds the Image for Grav using the official Dockerfile.
See https://github.com/getgrav/docker-grav

## Usage

Pull the image using podman.

  podman pull registry.gitlab.com/fanks4/docker-grav:latest
